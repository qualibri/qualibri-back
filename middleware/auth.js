const jwt = require('jsonwebtoken');
const User = require('../models').Users;

const auth = async (req, res, next) => {
    try{
        const tokenAuth = req.header('Authorization').replace('Bearer ', '');
        const decoded = jwt.verify(tokenAuth, 'thisisqualibri');

        const user = await User.scope(req.query['scope']).findOne({ where: { id: decoded._id, user_token: tokenAuth } })

        if (!user){
            throw new Error()
        }


        req.token = tokenAuth 
        req.user = user
        next()

    }catch(e){
        res.status(401).send({error: 'Please auth Middleware' + e})
    }
}

module.exports = auth;
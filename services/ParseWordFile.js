const mammoth = require('mammoth')
const HTMLParser = require('node-html-parser')
const prettyjson = require('prettyjson')
const mammothOptions = {
    convertImage: mammoth.images.imgElement(function(image) {
        return image.read('base64').then(function(imageBuffer) {
            return {
                src: 'data:' + image.contentType + ';base64,' + imageBuffer
            }
        })
    })
}
const prettyjsonOptions = {
    keysColor: 'green',
    dashColor: 'red',
    stringColor: 'blue'
}

//---------------------------------------------------------------------------------------------------------
// var dir = require('node-dir')

// var files = new Promise(function(resolve, reject) {
//     dir.files('../sample/sample.txt', function(err, res) {
//         if (err) {
//             reject(err)
//             console.log('Oops, an error occured %s', err)
//         } else {
//             resolve(res)
//         }
//     })
// })

exports.files = files
//---------------------------------------------------------------------------------------------------------
module.exports = wordFile => {
    let wordParsed = {}

    // mammoth
    //     .convertToHtml({ path: './CRES-01-001.docx' }, mammothOptions)
    //     .then(function(result) {
    //         let html = result.value // The generated HTML
    //         // console.log(result.images)
    //         // var messages = result.messages; // Any messages, such as warnings during conversion
    //         const root = HTMLParser.parse(html)
    //         // console.log(prettyjson.render(root.childNodes, prettyjsonOptions));
    //         let wordFileParsed = ParseHtmlRule(root.childNodes)
    //         wordParsed = wordFileParsed
    //         // console.log(prettyjson.render(wordParsed, prettyjsonOptions))
    //     })
    //     .done()

    //---------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------

    function ParseHtmlRule(
        word,
        count = 0,
        previousField = { type: '', content: '' },
        wordFileParsed = {},
        ruleTitle = '',
        ruleSubtitle = ''
    ) {
        if (count == word.length) {
            // console.log(JSON.stringify(wordFileParsed, null, "  "));
            return wordFileParsed
        }
        // console.log(word[count].rawText)
        newRuleTitle = ruleTitle
        newRuleSubtitle = ruleSubtitle
        if (previousField.type == 'h1') {
            newRuleTitle = previousField.content == 'Introduction' ? 'introduction' : 'rule'

            wordFileParsed[newRuleTitle] =
                newRuleTitle == 'introduction'
                    ? { content: word[count].rawText }
                    : { title: previousField.content }
        } else if (previousField.type == 'h2') {
            newRuleSubtitle = previousField.content
            if (wordFileParsed['rule'][previousField.content]) {
                wordFileParsed['rule'][previousField.content].push([word[count].rawText])
            } else {
                wordFileParsed['rule'][newRuleSubtitle] = [word[count].rawText]
            }
        } else if (previousField.content == 'Exception : ') {
            wordFileParsed['rule'][newRuleSubtitle].push({
                Exeption: word[count].rawText
            })
        }
        if (word[count].childNodes[0].tagName == 'img') {
            console.log(word[count].childNodes[0].tagName)
            // console.log(word[count].childNodes[0].rawAttrs);
        }

        return ParseHtmlRule(
            word,
            count + 1,
            {
                type: word[count].tagName,
                content: word[count].rawText
            },
            wordFileParsed,
            newRuleTitle,
            newRuleSubtitle
        )
    }

    return wordParsed
}

'use strict'
module.exports = (sequelize, DataTypes) => {
    const Roles = sequelize.define(
        'Roles',
        {
            role_value: DataTypes.STRING,
            role_is_displayed: DataTypes.BOOLEAN
        },
        {}
    )
    Roles.associate = function(models) {
        // associations can be defined here
        Roles.hasMany(models.Users, { foreignKey: 'id_role' })
    }
    return Roles
}

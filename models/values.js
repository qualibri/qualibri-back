'use strict'
module.exports = (sequelize, DataTypes) => {
    const Values = sequelize.define(
        'Values',
        {
            value: DataTypes.STRING,
            fieldId: DataTypes.INTEGER
        },
        {}
    )
    Values.associate = function(models) {
        // associations can be defined here
        Values.belongsTo(models.Fields, { foreignKey: 'fieldId' })
    }
    return Values
}

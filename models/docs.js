'use strict'
module.exports = (sequelize, DataTypes) => {
    const Docs = sequelize.define(
        'Docs',
        {
            doc_name: DataTypes.STRING,
            doc_version: DataTypes.STRING,
            doc_revision: DataTypes.STRING,
            doc_date_create: DataTypes.DATE,
            doc_to_validate: DataTypes.BOOLEAN,
            doc_is_displayed: DataTypes.BOOLEAN
        },
        {}
    )
    Docs.associate = function(models) {
        // associations can be defined here
        Docs.hasMany(models.Rules)
    }
    return Docs
}

'use strict'
module.exports = (sequelize, DataTypes) => {
    const Fields = sequelize.define(
        'Fields',
        {
            field_name: DataTypes.STRING
        },
        {}
    )
    Fields.associate = function(models) {
        // associations can be defined here
    }
    return Fields
}

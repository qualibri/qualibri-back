'use strict'
module.exports = (sequelize, DataTypes) => {
    const Users_Rules = sequelize.define(
        'Users_Rules',
        {
            userId: DataTypes.INTEGER,
            ruleId: DataTypes.INTEGER
        },
        {}
    )
    Users_Rules.associate = function(models) {
        Users_Rules.belongsTo(models.Users, { foreignKey: 'userId' })
        Users_Rules.belongsTo(models.Rules, { foreignKey: 'ruleId' })
    }
    return Users_Rules
}

'use strict'
module.exports = (sequelize, DataTypes) => {
    const Images = sequelize.define(
        'Images',
        {
            image_title: DataTypes.STRING,
            image_content: DataTypes.TEXT('long')
        },
        {}
    )
    Images.associate = function(models) {
        models.Rules.hasMany(Images)
    }
    return Images
}

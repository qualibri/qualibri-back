'use strict'

module.exports = (sequelize, DataTypes) => {
    const Rules = sequelize.define(
        'Rules',
        {
            rule_title: DataTypes.STRING,
            rule_content: DataTypes.TEXT,
            rule_date_create: DataTypes.DATE,
            rule_is_displayed: DataTypes.BOOLEAN,
            rule_is_validate: DataTypes.BOOLEAN,
            rule_version: DataTypes.STRING,
            rule_revision: DataTypes.STRING
        },
        {}
    )

    Rules.associate = function(models) {
        // associations can be defined here
        Rules.belongsToMany(models.Users, { through: 'Users_Rules', foreignKey: 'ruleId' })
        Rules.belongsTo(Rules, { as: 'parentRule', through: models.Rules })
        // Rules.Image = Rules.hasMany(models.Image)
    }
    return Rules
}

'use strict'
module.exports = (sequelize, DataTypes) => {
    const Types = sequelize.define(
        'Types',
        {
            type_name: DataTypes.STRING
        },
        {}
    )
    Types.associate = function(models) {
        // associations can be defined here
        Types.hasMany(models.Fields)
    }
    return Types
}

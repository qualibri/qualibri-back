'use strict'
module.exports = (sequelize, DataTypes) => {
    const Users = sequelize.define(
        'Users',
        {
            user_name: DataTypes.STRING,
            user_status: DataTypes.STRING,
            user_is_displayed: DataTypes.BOOLEAN,
            user_email: DataTypes.STRING,
            user_password: DataTypes.STRING,
            user_token: DataTypes.STRING
        },
        {}
    )
    Users.associate = function(models) {
        // associations can be defined here
        // Users.belongsToMany(models.Rules, { through: 'rule_users' })
        Users.belongsToMany(models.Rules, { through: 'Users_Rules', foreignKey: 'userId' })
    }

    return Users
}

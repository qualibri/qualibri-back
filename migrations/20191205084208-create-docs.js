'use strict'
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Docs', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            doc_name: {
                type: Sequelize.STRING
            },
            doc_version: {
                type: Sequelize.STRING
            },
            doc_revision: {
                type: Sequelize.STRING
            },
            doc_date_create: {
                type: Sequelize.DATE
            },
            doc_to_validate: {
                type: Sequelize.BOOLEAN
            },
            doc_is_displayed: {
                type: Sequelize.BOOLEAN
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        })
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Docs')
    }
}

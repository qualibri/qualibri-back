'use strict'
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Rules', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            rule_title: {
                type: Sequelize.STRING
            },
            rule_content: {
                type: Sequelize.TEXT
            },
            rule_version: {
                type: Sequelize.STRING
            },
            rule_revision: {
                type: Sequelize.STRING
            },
            rule_date_create: {
                type: Sequelize.DATE
            },
            rule_is_displayed: {
                type: Sequelize.BOOLEAN
            },
            rule_is_validate: {
                type: Sequelize.BOOLEAN
            },
            DocId: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'Docs',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            },

            parentRuleId: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'Rules',
                    key: 'id'
                }
            }
        })
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Rules')
    }
}

'use strict'
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Fields', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            field_name: {
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            },

            type_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'Types',
                    key: 'id'
                }
            },
            ruleId: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'Rules',
                    key: 'id'
                }
            }
        })
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Fields')
    }
}

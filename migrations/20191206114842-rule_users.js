'use strict'

module.exports = {
    up: (queryInterface, Sequelize) => {
        // Product belongsToMany Tag
        return queryInterface.createTable('rule_users', {
            UserId: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'Users',
                    key: 'id'
                }
            },
            RuleId: {
                type: Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: 'Rules',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        })
    },

    down: (queryInterface, Sequelize) => {
        // remove table
        return queryInterface.dropTable('rule_users')
    }
}

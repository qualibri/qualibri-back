'use strict'
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Users', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },

            user_name: {
                type: Sequelize.STRING
            },
            user_status: {
                type: Sequelize.STRING
            },
            user_is_displayed: {
                type: Sequelize.BOOLEAN
            },
            user_email: {
                type: Sequelize.STRING
            },
            user_password: {
                type: Sequelize.STRING
            },
            user_token: {
                type: Sequelize.STRING
            },
            id_role: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Roles',
                    key: 'id'
                }
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        })
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Users')
    }
}

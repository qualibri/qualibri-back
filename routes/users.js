const User = require('../models').Users
const Role = require('../models').Roles
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const auth = require('../middleware/auth')

module.exports = (express, base, passport) => {
    const users = express.Router()
    //on ecrit ici out les roles necessaire a l'application
    let roles = [{ role_value: "admin"},
                { role_value: "ing"},
                { role_value: "tech"}]


    users.post('/create', async (req, res, next) => {
        try {
            let user = req.body
            let roleName = req.body.user_role
        
            // we used bcrypt
            let password = req.body.user_password
            user.user_password = await bcrypt.hash(password, 8)

            // on s'assure que les roles existes, et on les créer dans le cas contraire
            for(let role in roles){    
                isRoleUnique(roles[role].role_value).then(isUnique => {
                    if(isUnique){
                        Role.create(roles[role]);
                    } else {
                        console.log(roles[role].role_value,  ': exist');
                    }
                })
            } 
            
            // Recover role_id
            user.id_role = await Role.findOne({
                where: { role_value: roleName }
            }).then(role => { return role.id })
               
            1 

            // Create an User
            userId = await User.create(user)
            user.user_is_displayed = true

             // // Token
            const token = jwt.sign({ _id: userId.id}, 'thisisqualibri')
            user.user_token = token
            await User.update(user, { where: { id: userId.id } })

            res.status(201).json({ user })
        } catch (e) {
            next(e)
        }
    })
    

    users.post('/login', async (req, res, next) => {
        try {
            const user = await User.scope(req.query['scope']).findOne({
                where: { user_name: req.body.user_name }
            })

            const password = await req.body.user_password
            const pwd = await User.scope(req.query['scope'])
                .findOne({ where: { user_name: req.body.user_name } })
                .then(accountInMysql => accountInMysql.user_password)
                
                
           const isMatch = await bcrypt.compare(password, pwd)

            // My token
            const userId = await User.scope(req.query['scope'])
                .findOne({ where: { user_name: req.body.user_name } })
                .then(accountInMysql => accountInMysql.id)
            const token = jwt.sign({ _id: userId }, 'thisisqualibri')

            
            if (isMatch) {
                res.status(201).send({ user, token })
            }
        } catch (e) {}
    })

    users.post('/logout', async (req, res) => {
        try {
            const user = await User.scope(req.query['scope']).findOne({
                where: { user_name: req.body.user_name }
            })
            user.user_token = 'null';
        
            res.send()
        } catch (error) {
            res.status(500).send('logout error : ' + error)
            console.log('logout error : ' + error);
        }
    })

    users.get('/getUsers',  async (req, res) => {
        console.log("wait a moment getUsers run you're request...");
        
        try {
            const user = await User.scope(req.query['scope']).findAll({
                where: { user_is_displayed: true }
            })

            res.status(200).send(user)
        } catch (e) {
            res.status(500).send(e)
        }
    })


    users.get('/getUsers/me', auth,  async (req, res, next) => {
        res.send(req.user);
    })


    users.get('/:id', async (req, res, next) => {
        try {
            const user = await User.scope(req.query['scope']).findOne({
                where : { id: req.params['id'], user_is_displayed: true}
            })
            res.send(user)
        } catch (e) {
            res.status(500).send(e)
        }
    })

    // users.get('/:id', async (req, res, next) => {
    //     try {
    //         const user = await User.scope(req.query['scope']).findByPk(
    //             req.params['id']
    //         )
    //         res.send(user)
    //     } catch (e) {
    //         res.status(500).send(e)
    //     }
    // })

    users.put('/update/:id', async (req, res, next) => {
        try {
            await User.update(req.body, { where: { id: req.params['id'] } })
            res.sendStatus(200)
        } catch (e) {
            next(e)
        }
    })


    users.put('/delete/:id', async (req, res, next) => {
        try {
            await User.update({ user_is_displayed: false }, { where: { id: req.params['id'] } })
            res.sendStatus(200)
        } catch (e) {
            next(e)
        }
    })

    users.put('/:id', async (req, res, next) => {
        try {
            await User.update(req.body, { where: { id: req.params['id'] } })
            res.sendStatus(200)
        } catch (e) {
            next(e)
        }
    })

    // admin routes
    users.get('/getUsers/admin',  async (req, res) => {
        console.log("wait a moment getUsers for admins run you're request...");
        
        try {
            res.send(await User.findAll())
        } catch (e) {
            res.status(500).send(e)
        }
    })

    // manage roles
    function isRoleUnique(role) {
        return Role.count({
            where: {
                role_value: role
            }}).then( res => {
                if(res != 0){
                    return false
                }
            return true
        });
    }



    return users
}


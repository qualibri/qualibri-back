# BackQualibri

Ce projet front est réalisé en NodeJS avec Sequelize.

## Lancement du serveur

Lancer `npm install` pour installer tout les packages utilisés.

Lancer un serveur MySQL Server d'une quelconque manière.
Nous avons ici utilisé Mamp / Wamp

Aller dans le fichier de config et rentrer les informations d'une base de donnée locale MySQL.

Lancer `npx sequelize db:migrate` pour générer la base de donnée

Lancer simplement `nodemon .\index.js`


## Vérification du fonctionnement du serveur

Vérifier que le front est bien lancé

Lancer l'application sur votre navigateur

Créer un compte
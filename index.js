const colors = require('colors')
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const database = require('./database/database')
const parserWord = require('./module/Document/wordParser')

//Routes import
const users = require('./routes/users')(express, database)
const document = require('./module/Document/document.router')
const rule = require('./module/Rule/rule.router')

app.get('/', (req, res) => {
    res.send('Hello Qualibri')
})

// middleware for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// middleware for json body parsing
app.use(bodyParser.json({ limit: '5mb' }))

// enable corse for all origins
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Expose-Headers', 'x-total-count')
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH')
    res.header('Access-Control-Allow-Headers', 'Content-Type,authorization')

    next()
})

app.use('/users', users)
app.use('/document', document)
app.use('/rule', rule)

// parserWord().then(doc => {
//     console.log('THE DOC : ', doc)
// })

app.listen(3000, () => {
    console.log('Listening on port 3000 !'.rainbow.bold)
})

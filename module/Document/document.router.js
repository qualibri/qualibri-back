const express = require('express')
const controller = require('./document.controller')
var multer = require('multer')
var upload = multer({ dest: 'uploads/' })

const documentRouter = express.Router()
documentRouter
    .get('/', (req, res) => controller.findAll(req, res))
    .get('/more/:id', (req, res, next) => controller.findRulesByDocId(req, res, next))
    .get('/:id', (req, res, next) => controller.findDocumentById(req, res, next))
    .get('/find/:word', (req, res, next) => controller.findDocumentByAWord(req, res, next))
    .get('/findRule/:word', (req, res, next) => controller.findRuleByAWord(req, res, next))
    .post('/', (req, res, next) => controller.create(req, res, next))
    .post('/findRules', (req, res, next) => controller.findRuleByWords(req, res, next))
    .post('/upload', upload.single('file'), (req, res, next) => controller.upload(req, res, next))
    .put('/:id', (req, res) => controller.update(req, res))
    .delete('/:id', (req, res) => controller.delete(req, res))

module.exports = documentRouter

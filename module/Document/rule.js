class Rule {
    constructor(title, content, level = null, exeptions = [], images = []) {
        this.title = title
        this.content = content
        this.exeptions = exeptions
        this.level = level
        this.rules = []
        this.topRule = this.setTopRule()
        this.images = images
    }

    setTitle(title) {
        this.title = title
    }

    setcontent(content) {
        this.content = content
    }

    setLevel(level) {
        this.level = level
    }
    setTopRule(level) {
        const topRule = doc.superiorlevelrule(this.level)
        return topRule.title
    }

    addExeption(exeption = '') {
        this.exeptions.push({
            exeption_content: exeption
                .replace(/&gt/g, ' > ')
                .replace(/&lt/g, ' < ')
                .replace(/;/g, '')
        })
    }

    addImage(imageContent, imageTitle = '') {
        this.images.push({ image_title: imageTitle, image_content: imageContent })
    }
}

module.exports = Rule

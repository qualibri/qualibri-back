const moment = require('moment')
// const rule = require('./rule');
const Doc = require('../../models').Docs
const Rules = require('../../models').Rules
const Images = require('../../models').Images
const Exeptions = require('../../models').Exeptions
const { Op } = require('sequelize')

let nextId = 1

class RuleDao {
    findAll() {
        return Doc.findAll()
    }

    findDocumentById(id) {
        return Doc.findAll({
            where: {
                id: id
            },
            order: [['id', 'ASC']]
        })
    }

    findRulesByDocId(id) {
        return Rules.findAll({
            where: {
                DocId: id
            },
            order: [['id', 'ASC']]
        })
    }

    findDocumentByAWord(word) {
        return Doc.findAll({
            where: {
                doc_name: word
            }
        })
    }

    findRuleByAWord(word) {
        return Rules.findAll({
            where: {
                rule_content: { [Op.like]: '%' + word + '%' }
            }
        })
    }

    findRuleTitleByWord(word) {
        return Rules.findAll({
            where: {
                rule_tilte: { [Op.like]: '%' + word + '%' }
            }
        })
    }

    findRuleByWords(words) {
        // console.log(words)
        let tabWords = []
        words.forEach(word => {
            tabWords.push({ rule_content: { [Op.like]: '%' + word + '%' } })
        })
        return Rules.findAll({
            where: {
                [Op.and]: tabWords
            }
        })
    }

    create(rule) {
        rule.id = nextId++
        rules.push(rule)
        return rule
    }

    update(ruleToUpdate) {
        const index = Rules.findIndex(rule => rule.id === ruleToUpdate.id)
        if (index > -1) {
            rules[index] = ruleToUpdate
        }
    }

    delete(id) {
        const index = Rules.findIndex(rule => rule.id === id)
        rules.splice(index, 1)
    }

    async createDocument(document) {
        let docCheck = await Doc.findAll({
            limit: 1,
            where: {
                doc_name: document.documentTitle
            },
            order: [['id', 'DESC']]
        })

        // Pour l'instant on ne peut inserer que les docs plus récents
        if (docCheck) {
            for (const rule of docCheck) {
                console.log(rule.doc_version)
                if (rule.doc_version == document.version) {
                    console.log('This document is already in the database')
                    return
                }
            }
        }

        Doc.create(
            {
                doc_name: document.documentTitle,
                doc_version: document.version,
                doc_revision: document.revision,
                doc_date_create: moment(),
                doc_to_validate: true,
                doc_is_displayed: false
            },
            {
                include: Rules
            }
        ).then(async res => {
            const DocId = res.dataValues.id

            for (const rule of document.rules) {
                //check If exist
                let ruleId = await Rules.findOne({ where: { rule_title: rule.topRule ? rule.topRule : '' } })
                // console.log(rule.images)
                await Rules.create(
                    {
                        rule_title: rule.title,
                        rule_content: rule.content,
                        rule_version: document.version,
                        rule_revision: document.revision,
                        DocId: DocId,
                        rule_date_create: moment(),
                        rule_is_displayed: true,
                        rule_is_validate: true,
                        parentRuleId: ruleId ? ruleId.id : null,
                        Images: rule.images,
                        Exeptions: rule.exeptions
                    },
                    { include: [Images, Exeptions] }
                )
            }
        })
    }

    updateDisplayStatus(ruleTitle, version) {
        Rules.findOne({
            where: {
                rule_title: ruleTitle,
                rule_version: version
            }
        }).then(rep => {
            console.log(rep.dataValues.rule_is_displayed)
            rep.update({
                rule_is_displayed: rep.dataValues.rule_is_displayed ? false : true
            }).then(rep => {
                console.log(rep.dataValues.rule_is_displayed)
            })
        })
    }
}

const ruleDao = new RuleDao()
module.exports = ruleDao

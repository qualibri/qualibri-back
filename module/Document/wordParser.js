const document = require('./document')
const fs = require('fs-js')
const mammoth = require('mammoth')
const HTMLParser = require('node-html-parser')
const mammothOptions = {
    convertImage: mammoth.images.imgElement(function(image) {
        return image.read('base64').then(function(imageBuffer) {
            return {
                src: 'data:' + image.contentType + ';base64,' + imageBuffer
            }
        })
    })
}

//---------------------------------------------------------------------------------------------------------
module.exports = wordFilePath => {
    return new Promise((resolve, reject) => {
        doc = new document()

        mammoth
            .convertToHtml({ path: wordFilePath }, mammothOptions)
            .then(result => {
                fs.unlink(wordFilePath, err => {
                    if (err) {
                        console.log('failed to delete local image:' + err)
                    } else {
                        console.log('successfully deleted local image')
                    }
                })

                let html = result.value // The generated HTML
                const root = HTMLParser.parse(html)
                ParseHtmlRule(root.childNodes)
            })
            .done(() => {
                resolve(doc)
            })

        //---------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------

        function ParseHtmlRule(word, count = 0, previousField = { type: '', content: '' }, intro = false) {
            if (count == word.length) {
                return
            }
            // console.log('test-----------<> ', word[count].rawText, '   ', word[count].tagName)
            word[count].rawText

            if (previousField.type == 'h1') {
                if (previousField.content == 'Introduction') {
                    doc.setIntro(word[count].rawText)
                    intro = true
                } else {
                    console.log('h1 ------> ', previousField.content)
                    doc.addRule(previousField.content, null, 0)
                }
            } else if (previousField.type == 'h2') {
                //console.log('level : ' + previousField.content.match(/\./g).length)
                doc.addRule(previousField.content, word[count].rawText, previousField.content.match(/\./g).length)
                doc.superiorlevelrule(previousField.content.match(/\./g).length)
                // console.log(doc)
            } else if (previousField.content == 'Exception : ') {
                word[count].childNodes.forEach(key => {
                    doc.rules[doc.rules.length - 1].addExeption(key.rawText)
                })
            } else if (count === 0) {
                doc.setDocumentTitle(word[count].childNodes[0].rawText)
            } else if (word[count].childNodes[0].tagName == 'img') {
                doc.rules[doc.rules.length - 1].addImage(word[count].childNodes[0].rawAttrs.match(/(?<=base64,).(.*).(?=")/)[0])
            } else if (word[count].childNodes[0].rawText.includes('Version')) {
                word[count].childNodes.forEach(key => {
                    if (key.rawText.includes('Version')) {
                        key.rawText.replace('Version ', '')
                        doc.setVersion(parseInt(key.rawText.replace('Version ', '')))
                    } else if (key.rawText.includes('Révision')) {
                        doc.setRevision(parseInt(key.rawText.replace('Révision ', '')))
                    }
                })
            } else if (word[count].childNodes[0].tagName == 'tr') {
                //-------------------------------line-----------col------------------value
                // console.log(word[count].childNodes[0].childNodes[0].childNodes[0].rawText, ' --> ', word[count].childNodes[0].tagName)
            } else if (intro) {
                // console.log('oijoijoijoijoijoi ----->', word[count].rawText)
                if (word[count + 1].tagName != 'p') {
                    intro = false
                } //-------------------------------line-----------col------------------value
                // console.log(word[count].childNodes[0].childNodes[0].childNodes[0].rawText, ' --> ', word[count].childNodes[0].tagName)
            } else {
                // console.log(word[count].childNodes[0], ' --> ', word[count].childNodes[0].tagName)
            }

            return ParseHtmlRule(
                word,
                count + 1,
                {
                    type: word[count].tagName,
                    content: word[count].rawText
                },
                intro
            )
        }
    })
}

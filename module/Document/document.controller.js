const dao = require('./document.dao')
const parserWord = require('./wordParser')

class documentController {
    async findAll(req, res) {
        var rule = await dao.findAll()
        res.send(rule)
    }

    async findDocumentById(req, res, next) {
        const documentToFind = req.params.id
        var document = await dao.findDocumentById(documentToFind)
        res.send(document)
    }

    async findRulesByDocId(req, res, next) {
        const documentToFind = req.params.id
        var document = await dao.findRulesByDocId(documentToFind)
        res.send(document)
    }

    findDocumentByAWord(req, res, next) {
        const wordToFind = req.params.word
        dao.findDocumentByAWord(wordToFind).then(rules => {
            res.json(rules)
        })

        // if (rules) {
        //     res.json(rules)
        // } else {
        //     // next({
        //     //     type: 'RESOURCE_NOT_FOUND',
        //     //     message: `rule with id=${id} does not exist`
        //     // })
        //     console.log('not found')
        // }
    }

    findRuleByAWord(req, res, next) {
        const wordToFind = req.params.word
        dao.findRuleByAWord(wordToFind).then(rules => {
            res.json(rules)
        })
    }
    findRuleByWords(req, res, next) {
        const wordToFind = req.body.words
        dao.findRuleByWords(wordToFind).then(rules => {
            res.json(rules)
        })
    }

    findRuleTitleByWord(req, res, next) {
        const wordToFind = req.param.word
        dao.findRuleTitleByWord(wordToFind).then(rules => {
            res.json(rules)
        })
    }

    create(req, res, next) {
        const ruleToCreate = req.body
        if (!ruleToCreate.name) {
            next({
                type: 'INVALID_RESOURCE',
                message: `rule cannot be created without name`
            })
            return
        }
        const createdrule = dao.create(ruleToCreate)
        res.status(201)
        res.json(createdrule.id)
    }

    update(req, res) {
        // dao.update(req.body)
        switch (req.body.to_update) {
            case 'display_rule':
                dao.updateDisplayStatus(req.body.rule_title, req.body.rule_version)
                break

            default:
                break
        }

        res.status(204)
        res.json()
    }

    delete(req, res) {
        dao.delete(parseInt(req.params.id))
        res.status(204)
        res.send()
    }

    upload(req, res) {
        console.log(req.file.path)
        parserWord(req.file.path).then(doc => {
            dao.createDocument(doc)
        })

        res.status(200)
        res.json({ message: 'File Uploaded' })
    }
}

const documentCtrl = new documentController()

module.exports = documentCtrl

const rule = require('./rule')

class Document {
    constructor() {
        this.documentTitle = ''
        this.version = 1
        this.revision = 1
        this.intro = ''
        this.ruleTitle = ''
        this.rules = []
    }

    setDocumentTitle(docTitle) {
        this.documentTitle = docTitle
    }

    setVersion(version) {
        this.version = version
    }

    setRevision(revision) {
        this.revision = revision
    }

    setIntro(introContent) {
        this.intro = introContent
    }

    setRuleTitle(ruleTitle) {
        this.ruleTitle = ruleTitle
    }

    addRule(RuleTitle, content, level) {
        this.rules.push(new rule(RuleTitle, content, level))
    }

    superiorlevelrule(ruleLevel) {
        // find the last item of tab that have a level -1 of this rule
        // this.rules.push(new rule(RuleTitle, content))
        let tempRules = this.rules.slice(0)
        const topRule = tempRules.reverse().find(rule => rule.level == ruleLevel - 1)
        if (topRule) {
            return topRule
        } else {
            return 'helo'
        }
    }
}

module.exports = Document

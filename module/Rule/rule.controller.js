const dao = require('./rule.dao')

class documentController {
    async findAll(req, res) {
        var rule = await dao.findAll()
        res.send(rule)
    }

    async findById(req, res, next) {
        const ruleToFind = req.params.id
        var rule = await dao.findById(ruleToFind)
        res.send(rule)
    }

    findRulesByAWord(req, res, next) {
        const wordToFind = req.params.word
        dao.findRulesByAWord(wordToFind).then(rules => {
            res.json(rules)
        })
    }

    update(req, res) {
        dao.update(req.body)
        res.status(204)
        res.send()
    }

    delete(req, res) {
        dao.delete(parseInt(req.params.id))
        res.status(204)
        res.send()
    }
}

const documentCtrl = new documentController()

module.exports = documentCtrl

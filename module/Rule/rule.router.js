const express = require('express')
const controller = require('./rule.controller')
var multer = require('multer')

const ruleRouter = express.Router()
ruleRouter
    .get('/', (req, res) => controller.findAll(req, res))
    .get('/:id', (req, res, next) => controller.findById(req, res, next))
    .put('/:id', (req, res) => controller.update(req, res))
    .delete('/:id', (req, res) => controller.delete(req, res))

module.exports = ruleRouter

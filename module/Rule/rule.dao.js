const moment = require('moment')
// const rule = require('./rule');
const Rules = require('../../models').Rules
const { Op } = require('sequelize')

let nextId = 1

class RuleDao {
    findAll() {
        return Rules.findAll()
    }

    findById(id) {
        return Rules.findAll({
            where: {
                DocId: id
            },
            order: [['id', 'ASC']]
        })
    }

    update(ruleToUpdate) {
        const index = rules.findIndex(rule => rule.id === ruleToUpdate.id)
        if (index > -1) {
            rules[index] = ruleToUpdate
        }
    }

    delete(id) {
        const index = rules.findIndex(rule => rule.id === id)
        rules.splice(index, 1)
    }
}

const ruleDao = new RuleDao()
module.exports = ruleDao
